//
//  TicketListViewModel.swift
//  TicketList
//
//  Created by Marek Šťovíček on 05.08.2022.
//

import Foundation

class TicketListViewModel: ObservableObject {

    @Published var content: [TicketListCellContent] = []

    func loadData() {
        content = [
            .init(
                date: "22. 7. 2022",
                time: "09:00",
                ticketType: "SOLO",
                sport: "football",
                team1: "iOS guys",
                team2: "android guys",
                description: "Lets play the game",
                stakeValue: Float.random(in: 0...500).rounded(),
                oddValue: Float.random(in: 1.5...10.0).rounded(),
                winValue: Float.random(in: 0...1000).rounded(),
                ticketStatus: .win
            ),
            .init(
                date: "22. 7. 2022",
                time: "09:00",
                ticketType: "SOLO",
                sport: "football",
                team1: "iOS guys",
                team2: "android guys",
                description: "Lets play the game",
                stakeValue: Float.random(in: 0...500).rounded(.down),
                oddValue: Float.random(in: 1.5...10.0).rounded(),
                winValue: Float.random(in: 0...1000).rounded(),
                ticketStatus: .win
            ),
            .init(
                date: "22. 7. 2022",
                time: "09:00",
                ticketType: "SOLO",
                sport: "football",
                team1: "iOS guys",
                team2: "android guys",
                description: "Lets play the game",
                stakeValue: Float.random(in: 0...500).rounded(),
                oddValue: Float.random(in: 1.5...10.0).rounded(),
                winValue: Float.random(in: 0...1000).rounded(),
                ticketStatus: .win
            ),
            .init(
                date: "22. 7. 2022",
                time: "09:00",
                ticketType: "SOLO",
                sport: "football",
                team1: "iOS guys",
                team2: "android guys",
                description: "Lets play the game",
                stakeValue: Float.random(in: 0...500).rounded(),
                oddValue: Float.random(in: 1.5...10.0).rounded(),
                winValue: Float.random(in: 0...1000).rounded(),
                ticketStatus: .win
            )
        ]
    }
}

struct TicketListCellContent: Hashable, Identifiable {
    let id: String = UUID().uuidString

    let date: String
    let time: String
    let ticketType: String
    let sport: String
    let team1: String
    let team2: String
    let description: String
    let stakeValue: Float
    let oddValue: Float
    let winValue: Float
    let ticketStatus: Status

    enum Status {
        case win
        case lose
    }
}
